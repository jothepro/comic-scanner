#!/usr/bin/env python3

import sys, getopt, cv2, numpy
from pathlib import Path
from matplotlib import pyplot as plt

def main(argv):
   inputDirectory = Path('./example-input')
   outputDirectory = Path('./example-output')
   imageThreshold = 35
   noiseReduction = 5
   centerCorridor = 40
   insetCorridor = 75
   calibrationMode = False

   helpText = 'comic-scanner.py -i <inputDirectory> -o <outputDirectory> -t <threshold>'
   try:
      opts, args = getopt.getopt(argv,"hi:o:t:n:c:m:i:",["inputDirectory=","outputDirectory=", "threshold=", "noiseReduction=", "centerCorridor=", "calibrationMode=", "insetCorridor="])
   except getopt.GetoptError:
      print(helpText)
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print (helpText)
         sys.exit()
      elif opt in ("-i", "--inputDirectory"):
         inputDirectory = Path(arg)
      elif opt in ("-o", "--outputDirectory"):
         outputDirectory = Path(arg)
      elif opt in ("-t", "--threshold"):
         imageThreshold = int(arg)
      elif opt in ("-n", "--noiseReduction"):
         noiseReduction = int(arg)
      elif opt in ("-c", "--centerCorridor"):
         centerCorridor = int(arg)
      elif opt in ("-m", "--calibrationMode"):
         calibrationMode = bool(arg)
      elif opt in ("-i", "--insetCorridor"):
         insetCorridor = int(arg)

   resultImageCounter = 0

   # iterate over all input files
   for resource in inputDirectory.iterdir():
      # exit after first image in calibration mode
      if calibrationMode and resultImageCounter > 5:
         exit()
      # iterate over all jpeg images in folder
      if resource.is_file() and resource.suffix in ['.jpg', '.JPG', '.jpeg', '.JPEG']:
         print('processing {0}...'.format(str(resource.absolute())))
         # load image into opencv and rotate it to face upwards
         originalImage = cv2.rotate(cv2.imread(str(resource.absolute())), cv2.ROTATE_180)
         originalImageHeight, originalImageWidth, originalImageChannels = originalImage.shape 
         scalingFactor = 500 / originalImageWidth
         scaledOriginalImage = cv2.resize(originalImage, (0, 0), fx=scalingFactor, fy=scalingFactor) 
         borderThickness = noiseReduction * 4
         originalImageWithBorder = cv2.copyMakeBorder(scaledOriginalImage, borderThickness, borderThickness, borderThickness, borderThickness, cv2.BORDER_CONSTANT)
         # convert to grayscale
         grayscaleImage = cv2.cvtColor(originalImageWithBorder, cv2.COLOR_BGR2GRAY)

         # calculate threshold on image
         ret,thresholdedImage = cv2.threshold(grayscaleImage,imageThreshold,255,cv2.THRESH_BINARY)

         # reduce noise in threshold
         kernel = numpy.ones((2,2),numpy.uint8)
         dilatedImage = cv2.dilate(thresholdedImage, kernel, iterations=noiseReduction)
         erodedImage = cv2.erode(dilatedImage, kernel, iterations=noiseReduction * 2)
         dilatedAgainImage = cv2.dilate(erodedImage, kernel, iterations=noiseReduction)

         # shift image upwards and left to correct shift by dilate & erode functions
         # translation matrix
         # If the shift is (x, y) then matrix would be 
         # M = [1 0 x] 
         #     [0 1 y] 
         offset = - noiseReduction * 2
         translationMatrix = numpy.float32([[1, 0, offset], [0, 1, offset]]) 
         (dilatedAgainImageRows, dilatedAgainImageCols) = dilatedAgainImage.shape[:2] 

         centerX = dilatedAgainImageRows / 2
         centerY = dilatedAgainImageCols / 2
  
         # warpAffine does appropriate shifting given the 
         # translation matrix. 
         finalImageMask = cv2.warpAffine(dilatedAgainImage, translationMatrix, (dilatedAgainImageCols, dilatedAgainImageRows)) 

         # detect corners
         cornerDetectionMask = cv2.cornerHarris(finalImageMask, 2, 3, 0.04)
         
         # generate list from all detected corners
         cornersList = []
         centerCornersList = []
         def loopOverRow(range):
            for col in range:
               if cornerDetectionMask[row][col] > 0.5*cornerDetectionMask.max():
                  cornersList.append((row,col))
               elif cornerDetectionMask[row][col] < 0.05*cornerDetectionMask.max() and cornerDetectionMask[row][col] > 0.02*cornerDetectionMask.max():
                  centerCornersList.append((row,col))
         for row in range(0, insetCorridor):
            loopOverRow(range(0, insetCorridor))
            loopOverRow(range(int(centerY - centerCorridor / 2), int(centerY + centerCorridor / 2)))
            loopOverRow(range(dilatedAgainImageCols - insetCorridor, dilatedAgainImageCols))
         for row in range(dilatedAgainImageRows - insetCorridor, dilatedAgainImageRows):
            loopOverRow(range(0, insetCorridor))
            loopOverRow(range(int(centerY - centerCorridor / 2), int(centerY + centerCorridor / 2)))
            loopOverRow(range(dilatedAgainImageCols - insetCorridor, dilatedAgainImageCols))
         # find outer corners
         topLeft = (sys.maxsize,sys.maxsize)
         topRight = (sys.maxsize, 0)
         bottomLeft = (0,sys.maxsize)
         bottomRight = (0,0)

         
         for corner in cornersList:
            x, y = corner
            topLeftX, topLeftY = topLeft
            topRightX, topRightY = topRight
            bottomLeftX, bottomLeftY = bottomLeft
            bottomRightX, bottomRightY = bottomRight
            if (x < topLeftX or y < topLeftY) and x < insetCorridor and y < insetCorridor:
               topLeft = corner
            if (x < topRightX or y > topRightY) and x < insetCorridor and y > dilatedAgainImageCols - insetCorridor:
               topRight = corner
            if (x > bottomLeftX or y < bottomLeftY) and x > dilatedAgainImageRows - insetCorridor and y < insetCorridor:
               bottomLeft = corner
            if (x > bottomRightX or y > bottomRightY) and x > dilatedAgainImageRows - insetCorridor and y > dilatedAgainImageCols - insetCorridor:
               bottomRight = corner
         
         # manipulate corners to cut a little bit more margin
         topLeft = (topLeft[0] + 2, topLeft[1] + 2)
         topRight = (topRight[0] + 2, topRight[1] - 2)
         bottomRight = (bottomRight[0] - 2, bottomRight[1] - 2)
         bottomLeft = (bottomLeft[0] - 2, bottomLeft[1] + 2)

         print("outer corners:")
         print("top left: {0}".format(topLeft))
         print("top right: {0}".format(topRight))
         print("bottom left: {0}".format(bottomLeft))
         print("bottom right: {0}".format(bottomRight))

         # find center corners

         topMiddle = (centerX, sys.maxsize)
         bottomMiddle = (centerX,0)

         # reduce points to take into consideration by only accepting corners close to the center of the image
         topCenterCornersList = []
         bottomCenterCornersList = []
         for corner in centerCornersList:
            x, y = corner
            if y > (centerY - centerCorridor / 2) and y < (centerY + centerCorridor / 2) and (x < insetCorridor or x > dilatedAgainImageRows - insetCorridor):
               if x < insetCorridor:
                  topCenterCornersList.append(corner)
               elif x > dilatedAgainImageRows - insetCorridor:
                  bottomCenterCornersList.append(corner)

         # find lowest point in topCenterCornersList
         topCenterCornerHigestX = 0
         for corner in topCenterCornersList:
            x, y = corner
            if x > topCenterCornerHigestX:
               topCenterCornerHigestX = x
         # find center between the two points
         topCenterCornerYCoordinates = []
         for corner in topCenterCornersList:
            x, y = corner
            if x == topCenterCornerHigestX:
               topCenterCornerYCoordinates.append(y)
         topCenterCornerY = (topCenterCornerYCoordinates[0] + topCenterCornerYCoordinates[len(topCenterCornerYCoordinates) - 1]) / 2
         topCenter = (topCenterCornerHigestX + 3, int(topCenterCornerY))

         # find heighest point in bottomCenterCornersList
         bottomCenterCornerLowestX = sys.maxsize
         for corner in bottomCenterCornersList:
            x, y = corner
            if x < bottomCenterCornerLowestX:
               bottomCenterCornerLowestX = x 
         bottomCenterCornerYCoordinates = []
         for corner in bottomCenterCornersList:
            x, y = corner
            if x == bottomCenterCornerLowestX:
               bottomCenterCornerYCoordinates.append(y)
         bottomCenterCornerY = (bottomCenterCornerYCoordinates[0] + bottomCenterCornerYCoordinates[len(bottomCenterCornerYCoordinates) - 1]) / 2
         bottomCenter = (bottomCenterCornerLowestX - 3, int(bottomCenterCornerY))

         print("center corners:")
         print("top center: {0}".format(topCenter))
         print("bottom center: {0}".format(bottomCenter))

         if calibrationMode:
            # visualize detected corners and preset corridors
            originalImageWithBorder[cornerDetectionMask>0.05*cornerDetectionMask.max()]=[0,0,255]
            originalImageWithBorder[ : , int(centerY - centerCorridor / 2)]=[255,0,0]
            originalImageWithBorder[ : , int(centerY + centerCorridor / 2)]=[255,0,0]
            originalImageWithBorder[ insetCorridor, : ] = [0,255,0]
            originalImageWithBorder[ dilatedAgainImageRows - insetCorridor, : ] = [0,255,0]
            originalImageWithBorder[ : , insetCorridor ] = [0,255,0]
            originalImageWithBorder[ : , dilatedAgainImageCols - insetCorridor] = [0,255,0]
            originalImageWithBorder[topCenter] = [255,255,0]
            originalImageWithBorder[bottomCenter] = [255,255,0]
            originalImageWithBorder[topLeft] = [255,255,0]
            originalImageWithBorder[topRight] = [255,255,0]
            originalImageWithBorder[bottomRight] = [255,255,0]
            originalImageWithBorder[bottomLeft] = [255,255,0]
            plt.rcParams["figure.figsize"] = (11,9)
            plt.title(resource.name, loc='center')
            plt.imshow(cv2.cvtColor(originalImageWithBorder, cv2.COLOR_BGR2RGB))
            plt.show()
            resultImageCounter = resultImageCounter + 2
         else:
            def rescalePoint(point):
               return ((point[0] - borderThickness) / scalingFactor, (point[1] - borderThickness) / scalingFactor)
            cv2.imwrite(str((outputDirectory / "img_{0}.jpg".format(str(resultImageCounter).zfill(3))).absolute()), unwarp(originalImage, rescalePoint(topLeft), rescalePoint(topCenter), rescalePoint(bottomCenter), rescalePoint(bottomLeft)))
            resultImageCounter = resultImageCounter + 1
            cv2.imwrite(str((outputDirectory / "img_{0}.jpg".format(str(resultImageCounter).zfill(3))).absolute()), unwarp(originalImage, rescalePoint(topCenter), rescalePoint(topRight), rescalePoint(bottomRight), rescalePoint(bottomCenter)))
            resultImageCounter = resultImageCounter + 1

def unwarp(image, topLeft, topRight, bottomRight, bottomLeft):
   # convert coordinates to what is required for usage in opencv
   tl = (topLeft[1], topLeft[0])
   tr = (topRight[1], topRight[0])
   br = (bottomRight[1], bottomRight[0])
   bl = (bottomLeft[1], bottomLeft[0])
   # compute the width of the new image, which will be the
   # maximum distance between bottom-right and bottom-left
   # x-coordiates or the top-right and top-left x-coordinates
   widthA = numpy.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
   widthB = numpy.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
   maxWidth = max(int(widthA), int(widthB))
   # compute the height of the new image, which will be the
   # maximum distance between the top-right and bottom-right
   # y-coordinates or the top-left and bottom-left y-coordinates
   heightA = numpy.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
   heightB = numpy.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
   maxHeight = max(int(heightA), int(heightB))
   # now that we have the dimensions of the new image, construct
   # the set of destination points to obtain a "birds eye view",
   # (i.e. top-down view) of the image, again specifying points
   # in the top-left, top-right, bottom-right, and bottom-left
   # order
   dst = numpy.array([
      [0, 0],
      [maxWidth - 1, 0],
      [maxWidth - 1, maxHeight - 1],
      [0, maxHeight - 1]], dtype = "float32")
   # compute the perspective transform matrix and then apply it
   M = cv2.getPerspectiveTransform(numpy.array([tl, tr, br, bl], dtype = "float32"), dst)
   return cv2.warpPerspective(image, M, (maxWidth, maxHeight))



if __name__ == "__main__":
   main(sys.argv[1:])